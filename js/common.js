var navMenu=document.querySelector('#nav-menu');
document.querySelector('#like-counter').onclick= function () {
    document.querySelector('.like').classList.add("rotate");
    setTimeout(function (){document.querySelector('.onemore').classList.add("arise");}, 500);
};

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    var elems=document.querySelectorAll('.plink');
    var elemsMobile=document.querySelectorAll('.plink-mobile');
    for(var i=0; i<elems.length; i++ ){
        elems[i].classList.add("hidden");
        elemsMobile[i].classList.add("visible");
    }
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

document.querySelector('#menu-toggle').onclick=function(){
    this.classList.toggle('open');
    document.querySelector('#like-counter').classList.toggle('hidden');
    if (hasClass(navMenu, 'hidden')) {
        navMenu.classList.toggle('hidden');
        setTimeout(function () {
            document.querySelector('#aside-menu').classList.add('bigger');
            document.querySelector('#menu-list').classList.remove('menu-back');
            navMenu.classList.remove('visuallyhidden');
        }, 20);
    }
    else {
        navMenu.classList.add('visuallyhidden');
        setTimeout(function () {
            document.querySelector('#aside-menu').classList.remove('bigger');
            document.querySelector('#menu-list').classList.add('menu-back');
        }, 20);
    }
};